const { response } = require('express');
const User = require('../models/User');
const Comment = require('../models/Comment');


//cria uma instancia do model
const create = async(req,res) => {
    try{
          const comment = await Comment.create(req.body);
          return res.status(201).json({message: "Comentário criado com sucesso!", comment: comment});
      }catch(err){
          res.status(500).json({error: err});
      }
};
//edita uma instancia ja criada da model
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não existe");
    }
};

//deleta uma instancia da model
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("O comentário foi apagado");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Comentário não existe");
    }
};
//pega todas as instancias ja criadas até o momento
const index = async(req,res) => {
    try {
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
};
//encontra uma instancia pelo id
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id, {
            include: [{
                model: User
            }]
        });
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }
};

const linkComment = async(req,res) => {
    const {CommentId,UserId } = req.params;

    try {
        const comment = await Comment.findByPk(CommentId);
        const user = await User.findByPk(UserId);
        await comment.setUser(user);
        return res.status(200).json({msg: "Comentário linkado a um usuário"});
    }catch(err){
        return res.status(500).json({err});
    }
}

const removeComment = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setUser(null);
        return res.status(200).json({msg: "comentário desvinculado"});
    }catch(err){
        return res.status(500).json({err});
    } 
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    linkComment,
    removeComment,
};
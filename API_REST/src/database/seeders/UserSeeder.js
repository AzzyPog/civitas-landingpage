const User = require("../../models/User");
const faker = require('faker-br');

const seedUser = async function () {
    try {
      await User.sync({ force: true });
      const users = [];

      for (let i = 0; i < 10; i++) {
 
       let user = await User.create({
         name: faker.name.firstName(),
         email: faker.internet.email(),
         password: faker.internet.password(),
         birthday: faker.date.between(1990, 2004),
         cellphone: faker.phone.phoneNumber(),
       });
 
 
     }
 
   } catch (err) { console.log(err); }
 }
 
 module.exports = seedUser;
const Comment = require("../../models/Comment");
const faker = require('faker-br');

const seedComment = async function () {
    try {
        await Comment.sync({ force: true });
        const comments = [];
    

        for (let i = 0; i < 10; i++) {
            let comment = await Comment.create({
                title: faker.name.title(),
                description: faker.lorem.text(),
                date: faker.date.recent(),
                likes: faker.random.number(30),
                location: faker.address.city(),

            });
            

        }

    } catch (err) { console.log(err); }
}

module.exports = seedComment;
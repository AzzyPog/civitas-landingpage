# <strong>Civitas project (Renato Longo Filho)

## || Conteúdo do projeto e     linguagens utilizadas
<br>

- JavaScript/DOM
- HTML
- CSS
- HTTP protocol
- CRUD - HTTP methods
- Fetch API

<hr>

<hr>

## || Rodando o projeto

<br>

#### ➡️ 1º - Clone esse repositório

```
https://gitlab.com/AzzyPog/civitas-landingpage.git
```

#### ➡️ 2º - Entre na pasta do projeto

```
civitas-landingpage/landingpage
```

#### ➡️ 3º - Abra o seu terminal (de preferência o do VScode ou do git) e execute o comando para instalar as dependências

```
npm install
```

#### ➡️ 4º - Execute o comando para iniciar a API

```
npx json-server --watch db.json
```

#### ➡️ 5º - Abra o arquivo `index.html` ou `register.html` na pasta /screens para iniciar a aplicação
<hr>
OBS: É extremamente recomendado a utilização da extensão "Live server" ou semelhante, para que as requisições assíncronas funcionem corretamente.
<hr>
<br>

#### ➡️ 6º - Caso abra `index.html`, aperte em **cadastro** para ir a pagina de registro
<br>

#### ➡️ 7º - Para parar a API, apenas execute o comando `CTRL + C` no terminal

<hr>

Created by Renato Longo Filho
